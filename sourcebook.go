package sourcebook

// the Sourcebook struct is used to reference where game elements are
// found and which rule editions are used in governing game entities
type Sourcebook struct {
  refID string          //a unique ID to use when referencing the source
  title string          //e.g., The Book of Chantries
  pubYear date          //4/1/999, etc.
  publisher string      //white wolf, black dog, etc.
  series string         //vtm, wta, sc, dtf, etc.
  seriesNumber int      //e.g., the ones found on the spines of MtA
  rulesEdition string   //first, second, etc.
}


